<?php

/* @var $this yii\web\View */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-logo">
    <a href="../../index2.html"><b>Admin</b></a>
</div>
<!-- /.login-logo -->
<div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <!-- <form action="../../index3.html" method="post"> -->
      <?php $form = ActiveForm::begin(['id' => 'login-form', 'class'=>'form-control']); ?>
        <div class="col-md-12">
          <!-- <input type="email" class="form-control" placeholder="Email"> -->
          <?= $form->field($model, 'username')->textInput() ?>
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div> -->
        </div>
        <div class="col-md-12">
          <!-- <input type="password" class="form-control" placeholder="Password"> -->
          <?= $form->field($model, 'password')->input('password') ?>
          <!-- <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div> -->
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <!-- /.col -->
          <div class="col-4">
            <!-- <button type="submit" class="btn btn-primary btn-block">Sign In</button> -->
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
          </div>
          <!-- /.col -->
        </div>
      <!-- </form> -->
      <?php ActiveForm::end(); ?>
      <p class="mb-1">
        <a href="forgot-password.html">I forgot my password</a>
      </p>
      <!-- <p class="mb-0">
        <a href="register.html" class="text-center">Register a new membership</a>
      </p> -->
    </div>
    <!-- /.login-card-body -->
  </div>