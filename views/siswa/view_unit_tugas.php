<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Siswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <p>
                        <?= Html::a('Update', ['update-bagian', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </p>
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'nama',
                            'alamat:ntext',
                            'asal_sekolah',
                            'jurusan',
                            'nis',
                            'status',
                            'unit_tugas',
                        ],
                    ]) ?>
                </div>
            </div>    
        </div>
    </di>
</div>
