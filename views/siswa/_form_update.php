<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\UnitBagian;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                        <p> Input User</p>
                    </div>
                    <div class="card-body">
                        <div class="users-form">
                            <?php $form = ActiveForm::begin(); ?>
                            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

                            <?= $form->field($model, 'asal_sekolah')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'jurusan')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'nis')->textInput(['maxlength' => true]) ?>

                            <!-- <?= $form->field($model, 'unit_tugas')->textInput(['maxlength' => true]) ?> -->
                            <?php 
                                $alls = UnitBagian::find()->where(['not', ['available' => 0]])->all();
                                $listData = ArrayHelper::map($alls,'id_bagian','nama_bagian');
                                echo $form->field($model, 'unit_tugas')->dropDownList($listData,['prompt'=>'Pilih Unit Bagian']);
                            ?> 
                            <?php 
                                $user = Yii::$app->user->identity;   
                                if($user->role == 2) :
                            ?>
                                 <?= $form->field($model, 'status')->dropDownList(['daftar' => 'Daftar', 'diterima' => 'Diterima'],['prompt'=>'Pilih Status']); ?>

                            <?php endif; ?>

                            <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>