<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Siswa PKL';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1><?= Html::encode($this->title) ?></h1>
                <p>
                    <?= Html::a('input siswa', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
                <div class="card-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        // 'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'nis',
                            'nama',
                            'alamat:ntext',
                            'asal_sekolah',
                            'jurusan',
                            'status',
                            'unit_bagian.nama_bagian',
                            [
                                'format'=>'raw',
                                'value' => function($data){
                                return
                                        Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view','id'=>$data['id']], ['title' => 'view','class'=>'btn btn-success']).' '.
                                        Html::a('<span class="glyphicon glyphicon-pencil"></span> Update', ['update','id'=>$data['id']], ['title' => 'edit','class'=>'btn btn-info']).' '.
                                        Html::a('<span class="glyphicon glyphicon-arrow-down"></span> Delete', ['delete', 'id' => $data['id']], ['class' => 'btn btn-danger',
                                    ]);
                                }
                            ]
                        ],
                    ]); ?>
                </div>
        </div>    
    </div>    
</div>   

