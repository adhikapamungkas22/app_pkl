<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SiswaSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daftar Siswa PKL PerBagian';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <!-- <p>
                        <?= Html::a('update bagian siswa', ['create'], ['class' => 'btn btn-success']) ?>
                    </p> -->
                </div>
                    <div class="card-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                // 'id',
                                'nis',
                                'nama',
                                'alamat:ntext',
                                'asal_sekolah',
                                'unit_tugas',
                                'status',
                                'tgl_mulai_pkl',
                                [
                                    'format'=>'raw',
                                    'value' => function($data){
                                    return
                                        Html::a('<span class="glyphicon glyphicon-pencil"></span> Approve', ['update-status','id'=>$data['id']], ['title' => 'edit','class'=>'btn btn-info']);
                                    }
                                ]
                            ],
                        ]); ?>
                    </div>
            </div>    
        </div>    
    </div>   
</div>

