<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Sekolah;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                        <p> Input User</p>
                    </div>
                    <div class="card-body">
                        <div class="users-form">
                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                            <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>
                            <!-- <?php 
                                // $alls = Sekolah::find()->all();
                                // $listData = ArrayHelper::map($alls,'id','nama');
                                // echo $form->field($model, 'asal_sekolah')->dropDownList($listData,['prompt'=>'Pilih Sekolah']);
                            ?> -->
                            <?= $form->field($model, 'asal_sekolah')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'jurusan')->textInput(['maxlength' => true]) ?>

                            <?= $form->field($model, 'nis')->textInput(['maxlength' => true]) ?>
                            <?= $form->field($model, 'file')->fileInput() ?>

                            <?= $form->field($model,'tgl_surat')->widget(DatePicker::className(),['clientOptions' => ['dateFormat' => 'yyyy-mm-dd']])->textInput() ?>

                            <?php 
                                $user = Yii::$app->user->identity;   
                                if($user->role == 2) :
                            ?>
                            <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>
                            <?php endif; ?>
                            <!-- Super Admin -->
                            <?php 
                                $user = Yii::$app->user->identity;   
                                if($user->role == 1) :
                            ?>
                            <?= $form->field($model, 'status')->dropDownList(['daftar' => 'Daftar', 'diterima' => 'Diterima'],['prompt'=>'Pilih Status']); ?>
                            <?php endif; ?>
                            <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>