<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Siswa */

$this->title = 'Pembagian Tugas Siswa PKL nama siswa ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Siswa', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="siswa-update">

    <h3><?= Html::encode($this->title) ?></h3>
    <br>
    <?= $this->render('_form_update', [
        'model' => $model,
    ]) ?>

</div>
