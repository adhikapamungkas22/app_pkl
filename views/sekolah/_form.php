<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sekolah */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                        <p> Input Sekolah</p>
                    </div>
                    <div class="card-body">
                        <div class="users-form">
                        <?php $form = ActiveForm::begin(); ?>

                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'alamat')->textarea(['rows' => 6]) ?>

                        <?= $form->field($model, 'akreditas')->textInput(['maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
