<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SekolahSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sekolah';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <p>
                        <?= Html::a('Create Sekolah', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'id',
                        'nama',
                        'alamat:ntext',
                        'akreditas',
                        // 'user_id',
                        //'created_at',
                        //'created_by',
                        //'updated_at',
                        //'updated_by',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, app\models\Sekolah $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'id' => $model->id]);
                            }
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
