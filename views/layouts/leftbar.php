<?php
  use yii\helpers\Html;
  use yii\helpers\Url; 
  use app\models\User;
?>

<!-- Brand Logo -->
<a href="index3.html" class="brand-link">
<!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
    <?php echo Html::img('@web/image/usni.png', ['class' => 'brand-image img-circle elevation-3" style="opacity: .8']); ?>
    <span class="brand-text font-weight-light">Dashboard USNI</span>
</a>
<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
            <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image"> -->
            <?php echo Html::img('@web/AdminLTE/dist/img/avatar.png', ['class' => 'img-circle elevation-2']); ?>
        </div>
        <div class="info">
            <?php $user = Yii::$app->user->identity; if ($user->status == 10): ?>

            <a href="#" class="d-block">              
                Hi, <?= $user->username; ?>
                <br>
                <?php
                    switch ($user->role) {
                        case 1:
                            echo "Super Admin";
                            break;
                        case 2:
                            echo "Hi, Purek";
                            break;
                        default:
                        echo "Hi, Admin";
                            break;
                    }
                ?>
            </a>
            <?php endif; ?>
        </div>
        </div>
        <!-- SidebarSearch Form -->
        <div class="form-inline">
            <div class="input-group" data-widget="sidebar-search">
                <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">
            <a href="<?=Url::to(['/'])?>" class="nav-link active">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Dashboard
                    <!-- <i class="right fas fa-angle-left"></i> -->
                </p>
            </a>
        </li>
        <?php 
            $user = Yii::$app->user->identity;   
            if($user->role == 2) :
        ?>
        <li class="nav-item">
        <a href="<?= Url::to(['siswa/status']) ?>" class="nav-link">
            <i class="nav-icon fas fa-book"></i>
            <p>
            <p>Approval Siswa PKL</p>
            </p>
        </a>
        </li>
        <?php endif; ?>
        <?php 
            $user = Yii::$app->user->identity;   
            if($user->role == 1 ||  $user->role == 3) :
        ?>
        <li class="nav-item">
        <a href="<?=Url::to(['siswa/index'])?>" class="nav-link">
            <i class="nav-icon fas fa-table"></i>
            <p>
                Daftar Siswa PKL
            </p>
        </a>
        </li>
        <?php endif; ?>
        <?php 
            $user = Yii::$app->user->identity;   
            if($user->role == 1) :
        ?>
        <li class="nav-item">
        <a href="<?= Url::to(['siswa/unit']) ?>" class="nav-link">
            <i class="nav-icon fas fa-book"></i>
            <p>
            <p>Pembagian Unit PKL</p>
            </p>
        </a>
        </li>
        <?php endif; ?>
        <?php 
            $user = Yii::$app->user->identity;   
            if($user->role == 1 ||  $user->role == 3) :
        ?>
        <li class="nav-item">
        <a href="<?=Url::to(['result/index'])?>" class="nav-link">
            <i class="nav-icon fas fa-edit"></i>
            <p>
                Hasil Siswa PKL
            </p>
        </a>
        </li>
        <?php endif; ?>
        <?php 
            $user = Yii::$app->user->identity;   
            if($user->role == 1) :
        ?>
        <li class="nav-item">
        <a href="<?=Url::to(['unit-bagian/index'])?>" class="nav-link">
            <i class="nav-icon fas fa-columns"></i>
            <p>
            Unit Bagian
            </p>
        </a>
        </li>
        <?php endif; ?>
        <?php 
            $user = Yii::$app->user->identity;   
            if($user->role == 1) :
        ?>
        <li class="nav-item">
        <a href="<?=Url::to(['user/index'])?>" class="nav-link">
            <i class="nav-icon fas fa-user"></i>
            <p> Users</p>
        </a>
        </li>
        <?php endif; ?>
        <li class="nav-header">Logout</li>
        <li class="nav-item">
        <a href="<?=Url::to(['site/logout']) ?>" class="nav-link">
            <i class="fas fa-circle nav-icon"></i>
            <p>Logout</p>
        </a>
        </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->