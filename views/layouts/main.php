<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title>Aplikasi Penilaian</title>
    <?php $this->head() ?>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<?php $this->beginBody() ?>
<div class="wrapper">
    <div class="preloader flex-column justify-content-center align-items-center">
        <!-- <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60"> -->
    </div>
    <nav class="main-header navbar navbar-expand navbar-white navbar-light">
        <?= $this->render('header')?>
    </nav>
    <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <?= $this->render('leftbar')?>
    </aside>
    <div class="content-wrapper">
        <!-- <section class="content"> -->
            <!-- page start-->
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
            <?= $content ?>
            <!-- page end-->
        <!-- </section> -->
    </div>
    <footer class="main-footer">
        <?= $this->render('footer')?>
    </footer>
    <aside class="control-sidebar control-sidebar-dark" style="display: none;">
        <?= $this->render('aside')?>
    </aside>
</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
