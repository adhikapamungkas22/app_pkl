<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\HasilPkl */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Result Siswa PKL', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <p>
                    <?= Html::a('Update', ['update', 'id' => $model->id_result], ['class' => 'btn btn-primary']) ?>
                    <?= Html::a('Delete', ['delete', 'id' => $model->id_result], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                </p>

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id_result',
                        'nilai',
                        'status',
                        'start_pkl',
                        'created_at',
                        'updated_at',
                        'end_pkl',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
</div>