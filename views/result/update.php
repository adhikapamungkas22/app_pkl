<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HasilPkl */

$this->title = 'Update Hasil PKL: ' . $model->id_result;
$this->params['breadcrumbs'][] = ['label' => 'result', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_result, 'url' => ['view', 'id' => $model->id_result]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>

            </div>
        </div>
    </div>
</div>