<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Siswa;
use yii\jui\DatePicker;


/* @var $this yii\web\View */
/* @var $model app\models\HasilPkl */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                        <p>Nilai siswa PKL</p>
                    </div>
                    <div class="card-body">
                        <div class="result-form">
                            <?php $form = ActiveForm::begin(); ?>
                            <?php 
                                $allSiswa = Siswa::find()->where(['status'=>'diterima'])->all();
                                $listData = ArrayHelper::map($allSiswa,'id','nama');
                                echo $form->field($model, 'id_siswa')->dropDownList($listData,['prompt'=>'pilih siswa']);
                            ?>
                            
                           <?= $form->field($model, 'q1')->textInput(['maxlength' => true, 'id' => 'q1']) ?>
                           <?= $form->field($model, 'q2')->textInput(['maxlength' => true, 'id' => 'q2']) ?>
                           <?= $form->field($model, 'q3')->textInput(['maxlength' => true, 'id' => 'q3']) ?>
                           <?= $form->field($model, 'q4')->textInput(['maxlength' => true, 'id' => 'q4']) ?>
                           <?= $form->field($model, 'q5')->textInput(['maxlength' => true, 'id' => 'q5']) ?>
                           <?= $form->field($model, 'q6')->textInput(['maxlength' => true, 'id' => 'q6']) ?>
                           <?= $form->field($model, 'q7')->textInput(['maxlength' => true, 'id' => 'q7']) ?>
                           <?= $form->field($model, 'q8')->textInput(['maxlength' => true, 'id' => 'q8']) ?>
                           <?= $form->field($model, 'q9')->textInput(['maxlength' => true, 'id' => 'q9']) ?>
                           <?= $form->field($model, 'q10')->textInput(['maxlength' => true, 'id' => 'q10']) ?>

                           <!-- <?= $form->field($model, 'status_pkl')->dropDownList(['1' => 'Lulus', '0' => 'Tidak Lulus'],['prompt'=>'Pilih Status']); ?> -->

                           <?= $form->field($model,'end_pkl')->widget(DatePicker::className(),['clientOptions' => ['dateFormat' => 'yyyy-mm-dd']])->textInput() ?>

                            <div class="form-group">
                                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>