<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ResultSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Result Penilain Siswa PKL';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1><?= Html::encode($this->title) ?></h1>
                     <p>
                        <!-- <?= Html::a('Nilai Siswa PKL', ['create'], ['class' => 'btn btn-success']) ?> -->
                    </p>
                </div>
                    <div class="card-body">
                        <?= GridView::widget([
                            'dataProvider' => $dataProvider,
                            // 'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => 'yii\grid\SerialColumn'],
                                'siswa.id',
                                'siswa.nama',
                                'nilai',
                                'siswa.tgl_mulai_pkl',
                                'end_pkl',
                                'status_pkl',
                                'nilai_huruf',
                                'siswa.status',
                                //'updated_at',
                                [
                                    'format'=>'raw',
                                    'value' => function($data){
                                    return
                                        // Html::a('<span class="glyphicon glyphicon-eye-open"></span> View', ['view','id'=>$data['id']], ['title' => 'view','class'=>'btn btn-success']).' '.
                                       Html::a('<span class="glyphicon glyphicon-pencil"></span> nilai siswa', ['update','id'=>$data['id_result']], ['title' => 'edit','class'=>'btn btn-info']).' '.
                                        Html::a('<span class="glyphicon glyphicon-arrow-down"></span> Cetak surat', ['download', 'id' => $data['id_result']], ['class' => 'btn btn-success',
                                        ]);
                                    }
                                ]
                            ],
                        ]); ?>
                    </div>
            </div>    
        </div>    
    </div>   
</div>
