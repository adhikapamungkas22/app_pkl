<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\HasilPkl */
$this->title = 'nilai siswa pkl';
$this->params['breadcrumbs'][] = ['label' => 'result', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <h1><?= Html::encode($this->title) ?></h1>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
