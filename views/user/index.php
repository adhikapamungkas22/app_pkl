<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h1><?= Html::encode($this->title) ?></h1>
                    <p>
                        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
                    </p>
                </div>
                <div class="card-body">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],
                            'id',
                            'username',
                            'auth_key',
                            //'password_hash',
                            //'password_reset_token',
                            'email:email',
                            'role',
                            'created_at',
                            //'updated_at',
                            //'verification_token',
                            [
                                'class' => ActionColumn::className(),
                                'urlCreator' => function ($action, \app\models\User $model, $key, $index, $column) {
                                    return Url::toRoute([$action, 'id' => $model->id]);
                                },
                            ],
                        ],
                    ]); ?>
                </div>
            </div>    
        </div>    
    </div>   
</div>
