<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Register User';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                        <p> Input User</p>
                    </div>
                    <div class="card-body">
                        <div class="users-form">
                            <?php $form = ActiveForm::begin(); ?>
                                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                                <?= $form->field($model, 'role')->dropDownList([1 => 'SuperAdmin', 2 => 'Purek', 3=>'Admin'],['prompt'=>'Pilih Role User']); ?>
                                <div class="form-group">
                                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>