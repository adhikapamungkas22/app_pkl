<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UnitBagian */

$this->title = 'Update Unit Bagian: ' . $model->id_bagian;
$this->params['breadcrumbs'][] = ['label' => 'Unit Bagians', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_bagian, 'url' => ['view', 'id' => $model->id_bagian]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="unit-bagian-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
