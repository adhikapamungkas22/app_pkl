<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\UnitBagian */

$this->title = 'Create Unit Bagian';
$this->params['breadcrumbs'][] = ['label' => 'Unit Bagians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="unit-bagian-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
