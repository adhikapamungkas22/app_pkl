<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UnitBagianSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unit Bagians';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h1><?= Html::encode($this->title) ?></h1>
                <p>
                    <?= Html::a('Create Unit Bagian', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
            </div>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                    'id_bagian',
                    'nama_bagian',
                    'available',
                    [
                    'class' => ActionColumn::className(),
                    'urlCreator' => function ($action, app\models\UnitBagian $model, $key, $index, $column) {
                        return Url::toRoute([$action, 'id_bagian' => $model->id_bagian]);
                        }
                    ],
                    ],
                ]); 
            ?>

        </div>
    </div>
</div>
