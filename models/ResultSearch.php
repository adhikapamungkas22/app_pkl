<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\HasilPkl;

/**
 * ResultSearch represents the model behind the search form of `app\models\HasilPkl`.
 */
class ResultSearch extends HasilPkl
{
    public $nama;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_result', 'id_siswa'], 'integer'],
            [['nilai', 'status_pkl', 'start_pkl', 'created_at', 'updated_at', 'end_pkl', 'nama'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HasilPkl::find();
        $query->joinWith(['siswa']);
        $query->select('hasil_pkl.*, siswa.*');
        $query->where(['siswa.status' => 'diterima']);
        
        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        // $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_result' => $this->id_result,
            'id_siswa' => $this->id_siswa,
            'status_pkl'=> $this->status_pkl,
            'start_pkl' => $this->start_pkl,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'end_pkl' => $this->end_pkl,
            // 'nama' => $this->nama,
        ]);

        $query->andFilterWhere(['like', 'nilai', $this->nilai]);
        $query->andFilterWhere(['like', 'nama', $this->nama]);


        return $dataProvider;
    }
}
