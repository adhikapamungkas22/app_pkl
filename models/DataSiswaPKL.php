<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "data_siswa_pkl".
 *
 * @property int $Id
 * @property int $id_siswa
 * @property string|null $surat_pkl
 * @property string|null $tgl_mulai_pkl
 * @property string|null $tgl_berakhir_pkl
 * @property string|null $created_at
 * @property string|null $updated_at
 */
class DataSiswaPKL extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'data_siswa_pkl';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_siswa'], 'required'],
            [['id_siswa'], 'integer'],
            [['surat_pkl'], 'string'],
            [['tgl_mulai_pkl', 'tgl_berakhir_pkl', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'Id' => 'ID',
            'id_siswa' => 'Id Siswa',
            'surat_pkl' => 'Surat Pkl',
            'tgl_mulai_pkl' => 'Tgl Mulai Pkl',
            'tgl_berakhir_pkl' => 'Tgl Berakhir Pkl',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
