<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_bagian".
 *
 * @property int $id
 * @property string|null $nama_bagian
 */
class UnitBagian extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit_bagian';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama_bagian'], 'string', 'max' => 100],
            [['available'], 'integer', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_bagian' => 'ID',
            'nama_bagian' => 'Nama Bagian',
            'available'=> 'available',
        ];
    }
}
