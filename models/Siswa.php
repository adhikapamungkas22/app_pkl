<?php

namespace app\models;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

use Yii;

/**
 * This is the model class for table "siswa".
 *
 * @property int $id
 * @property string|null $nama
 * @property string|null $alamat
 * @property string|null $asal_sekolah
 * @property string|null $jurusan
 * @property string|null $nis
 */


class Siswa extends \yii\db\ActiveRecord
{
    public $file;
    public $tgl_surat;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa';
    }

    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    // update 1 attribute 'created' OR multiple attribute ['created','updated']
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_tugas'], 'integer'],
            [['alamat'], 'string'],
            [['nama', 'asal_sekolah', 'jurusan', 'nis', 'status', 'tgl_surat'], 'string', 'max' => 100],
            [['tgl_mulai_pkl','surat_pkl', 'tgl_surat'], 'string'],
            [['file'], 'file', 'skipOnEmpty' => true, 'extensions' => 'pdf'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            // 'id' => 'ID',
            'nama' => 'Nama',
            'alamat' => 'Alamat',
            'asal_sekolah' => 'Asal Sekolah',
            'jurusan' => 'Jurusan',
            'nis' => 'NIS',
            'status' => 'Status',
            'unit_tugas' => "Bagian Unit",
            'surat_pkl' => "Surat PKL",
            'tgl_mulai_pkl' => "TGL Mulai PKL",
            'tgl_surat'=> "Tgl Surat",
        ];
    }

    public function upload()
    {
        
        if ($this->validate()) {
            // $this->file->saveAs(Yii::getAlias('@web/').'uploads'.$this->file->baseName . '.' . $this->file->extension);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit_bagian()
    {
        return $this->hasOne(UnitBagian::className(), ['id_bagian' => 'unit_tugas']);

    }
}
