<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Siswa;
use app\models\User;



/**
 * SiswaSearch represents the model behind the search form of `app\models\Siswa`.
 */
class SiswaSearch extends Siswa
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nama', 'alamat', 'asal_sekolah', 'jurusan', 'nis', 'status', 'tgl_surat'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $user = Yii::$app->user->identity;
        if ($user->role == 2) {
            $query = Siswa::find()->where(['status'=>'daftar']);
        }elseif ($params["r"] == "siswa/unit") {
            $query = Siswa::find();
            $query->joinWith(['unit_bagian']);
            $query->select('siswa.*, unit_bagian.*');
            $query->where(['siswa.status' => 'diterima']);
        }else{
            $query = Siswa::find();
            $query->joinWith(['unit_bagian']);
            $query->select('siswa.*, unit_bagian.*');
            $query->where(['siswa.status' => 'diterima']);
        }   

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'alamat', $this->alamat])
            ->andFilterWhere(['like', 'asal_sekolah', $this->asal_sekolah])
            ->andFilterWhere(['like', 'jurusan', $this->jurusan])
            ->andFilterWhere(['like', 'nis', $this->nis])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
