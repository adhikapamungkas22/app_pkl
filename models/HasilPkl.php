<?php

namespace app\models;
use yii\behaviors\AttributeBehavior;
use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "hasil_pkl".
 *
 * @property int $id
 * @property int|null $id_siswa
 * @property string|null $nilai
 * @property string|null $start_pkl
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property string|null $end_pkl
 */
class HasilPkl extends \yii\db\ActiveRecord
{
    public $q1;
    public $q2;
    public $q3;
    public $q4;
    public $q5;
    public $q6;
    public $q7;
    public $q8;
    public $q9;
    public $q10;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hasil_pkl';
    }


    public function init()
    {

        parent::init();
        $this->q1 = 0;
        $this->q2 = 0;
        $this->q3 = 0;
        $this->q4 = 0;
        $this->q5 = 0;
        $this->q6 = 0;
        $this->q7 = 0;
        $this->q8 = 0;
        $this->q9 = 0;
        $this->q10 = 0;

    }


    public function behaviors()
    {
        return [
            [
                'class' => AttributeBehavior::className(),
                'attributes' => [
                    // update 1 attribute 'created' OR multiple attribute ['created','updated']
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at','updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at',
                ],
                'value' => new \yii\db\Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_siswa', 'nilai'], 'integer'],
            [['start_pkl', 'created_at', 'updated_at', 'end_pkl'], 'safe'],
            [['status_pkl', 'nilai_huruf'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_result' => 'ID',
            'id_siswa' => 'Nama Siswa',
            'nilai' => 'Nilai',
            'start_pkl' => 'Tanggal Mulai PKL',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'end_pkl' => 'Tanggal Berakhir PKL',
            'status_pkl'=>'Status',
            'q1' =>'Sikap/Sopan Santun',
            'q2' =>'Kedisiplinan',
            'q3' =>'Kesungguhan',
            'q4' =>'Kemampuan Bekerja Mandiri',
            'q5' =>'Kemampuan Mengemukakan Pendapat',
            'q6' =>'Ketelitian',
            'q7' =>'Kemampuan Mengemukakan Pendapat',
            'q8' =>'Kemampuan Menyerap Hal Baru',
            'q9' =>'Inisiatif dan Kreatifitas',
            'q10' =>'Kemampuan Pemberi Kerja Praktik',
        ];
    }


    public function resultSave($id)
    {
        if ($this->validate()) {
            $hasil = HasilPkl::findOne($id, $nilai, $status);
            $hasil->nilai = $this->nilai;
            
            if ($hasil->save()) {
                return $hasil;
            }
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSiswa()
    {
        return $this->hasOne(Siswa::className(), ['id' => 'id_siswa']);
    }
}



