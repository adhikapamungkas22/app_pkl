<?php

namespace app\controllers;

use Yii;
use app\models\Siswa;
use app\models\HasilPkl;
use app\models\UnitBagian;
use app\models\DataSiswaPKL;
use app\models\SiswaSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SiswaController implements the CRUD actions for Siswa model.
 */
class SiswaController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                ],
            ]
        );
    }

    /**
     * Lists all Siswa models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new SiswaSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Siswa model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Siswa model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Siswa();
        $model["status"]  = "daftar";
        // $modelPKL = new DataSiswaPKL();
        
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->file = UploadedFile::getInstance($model, 'file');
                $model["surat_pkl"] = $model->file->baseName . '.' . $model->file->extension;
                if ($model->upload() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                } 
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            // 'modelPKL' =>$modelPKL,
        ]);
    }

    public function actionUpdateBagian($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            $request = $this->request->post();
            if($request["Siswa"]["unit_tugas"] != ""){
                $updateUnitBagian = UnitBagian::find()->where(['id_bagian'=>$request["Siswa"]["unit_tugas"]])->One();
                $av = $updateUnitBagian->available - 1;
                Yii::$app->db->createCommand('UPDATE unit_bagian SET available ='. $av .' WHERE id_bagian ='. $updateUnitBagian->id_bagian .' AND available > 0')->execute();
            }
            return $this->redirect(['view-unit-tugas', 'id' => $model->id]);
        }

        return $this->render('update_unit', [
            'model' => $model,
        ]);
    }
    
    public function actionUnit()
    {
        $searchModel = new SiswaSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('siswa_unit', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionStatus()
    {
        $searchModel = new SiswaSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('siswa_status', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionUpdateStatus($id)
    {
        $model = $this->findModel($id);
        if ($this->request->isPost && $model->load($this->request->post())) {
            $model->tgl_mulai_pkl = date('Y-m-d', strtotime($model->tgl_mulai_pkl));
            if($model->status == "daftar" || $model->status == ""){
                $model->tgl_mulai_pkl = null;
            }

            $model->save();

            $pkl = new HasilPkl();
            $pkl["start_pkl"]  = $model->tgl_mulai_pkl;
            $pkl["id_siswa"] = $model->id;
            $pkl->save();

            return $this->redirect(['status', 'id' => $model->id]);
        }

        return $this->render('update_status', [
            'model' => $model,
        ]);
    }

     /**
     * Displays a single Siswa model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionViewUnitTugas($id)
    {
        return $this->render('view_unit_tugas', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Updates an existing Siswa model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Siswa model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Siswa model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Siswa the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Siswa::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
