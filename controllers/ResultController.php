<?php

namespace app\controllers;

use Yii;
use kartik\mpdf\Pdf;
use app\models\HasilPkl;
use app\models\ResultSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ResultController implements the CRUD actions for HasilPkl model.
 */
class ResultController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all HasilPkl models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $user = Yii::$app->user->identity;
        if ($user == ""){
            $this->layout ="login";
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }
        }
        
        $searchModel = new ResultSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single HasilPkl model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new HasilPkl model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new HasilPkl();
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->end_pkl = date('Y-m-d', strtotime($model->end_pkl));
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing HasilPkl model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post())) {
            
            $model->end_pkl = date('Y-m-d', strtotime($model->end_pkl));
            if($model->status == "daftar" || $model->status == ""){
                $model->end_pkl = null;
            }

            $post = $this->request->post();
            $p = $post["HasilPkl"];
            
            $nilaiRata = ($p["q1"] + $p["q2"]+ $p["q3"] + $p["q4"]+$p["q5"]+$p["q6"] + $p["q7"] + $p["q8"] + $p["q9"] + $p["q10"]) / 10;
            if ($nilaiRata > 85){
                $nilai_huruf = "A";
            }elseif ($nilaiRata < 85 && $nilaiRata > 70) {
                $nilai_huruf = "B";
            }elseif ($nilaiRata< 70 && $nilaiRata> 60) {
                $nilai_huruf = "C";
            }else{
                $nilai_huruf = "D";
            }
    
            if($nilaiRata < 60){
                $status = "Tidak Lulus";
            }else{
                $status = "Lulus";
            }

            $model->nilai = $nilaiRata;
            $model->nilai_huruf = $nilai_huruf;
            $model->status = $status;

            $model->save();
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionDownload($id)
    {
        $model = $this->findModel($id);
		return $this->render('_reportView', ['model'=>$model]);    
    }

    /**
     * Deletes an existing HasilPkl model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the HasilPkl model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return HasilPkl the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = HasilPkl::findOne(['id_result' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
